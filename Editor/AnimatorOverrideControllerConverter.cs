﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.Animations;
using UnityEngine;

namespace Makihiro.AnimatorControllerMultiplexer.Editor
{
    public class AnimatorOverrideControllerConverter : EditorWindow
    {
        private RuntimeAnimatorController _src;

        private void OnGUI()
        {
            _src =
                EditorGUILayout.ObjectField("SRC", _src, typeof(RuntimeAnimatorController), true) as
                    RuntimeAnimatorController;
            if (!GUILayout.Button("convert")) return;
            var srcPath = AssetDatabase.GetAssetPath(_src);
            var path = EditorUtility.SaveFilePanelInProject(
                "new Animator Controller",
                srcPath,
                "controller",
                "Please enter a file name to save the controller to");
            var dstController = Convert(_src, path);
            EditorUtility.SetDirty(dstController);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }

        [MenuItem("AnimatorTools/ToAnimator")]
        private static void ShowWindow()
        {
            var window = GetWindow<AnimatorOverrideControllerConverter>();
            window.titleContent = new GUIContent("ToAnimator");
            window.Show();
        }

        public static AnimatorController Convert(RuntimeAnimatorController src, string dstPath)
        {
            var ac = src as AnimatorController;
            if (ac != null)
            {
                var srcPath = AssetDatabase.GetAssetPath(ac);
                AssetDatabase.CopyAsset(srcPath, dstPath);
                return AssetDatabase.LoadAssetAtPath<AnimatorController>(dstPath);
            }

            var aoc = src as AnimatorOverrideController;
            if (aoc == null) throw new Exception($"unknown type: {src.GetType().Name}");

            var c = Convert(aoc.runtimeAnimatorController, dstPath);

            var kvList = new List<KeyValuePair<AnimationClip, AnimationClip>>(aoc.overridesCount);
            aoc.GetOverrides(kvList);
            var clipDict = new Dictionary<AnimationClip, AnimationClip>();
            foreach (var kv in kvList)
            {
                Debug.Log($"{kv.Key.name} {kv.Value.name}");
                if (kv.Value == null) continue;

                clipDict[kv.Key] = kv.Value;
            }

            return ReplaceAnimationClip(c, clipDict);
        }

        private static AnimatorController ReplaceAnimationClip(AnimatorController animatorController,
            Dictionary<AnimationClip, AnimationClip> clipDict)
        {
            var layers = animatorController.layers;
            for (var i = 0; i < layers.Length; i++)
                layers[i].stateMachine = ReplaceAnimationClip(layers[i].stateMachine, clipDict);
            animatorController.layers = layers;

            return animatorController;
        }

        private static AnimatorStateMachine ReplaceAnimationClip(AnimatorStateMachine fsm,
            Dictionary<AnimationClip, AnimationClip> clipDict)
        {
            var states = fsm.states;
            for (var i = 0; i < states.Length; i++)
                states[i].state.motion = ReplaceAnimationClip(states[i].state.motion, clipDict);
            fsm.states = states;

            var stateMachines = fsm.stateMachines;
            for (var i = 0; i < stateMachines.Length; i++)
                stateMachines[i].stateMachine = ReplaceAnimationClip(stateMachines[i].stateMachine, clipDict);
            fsm.stateMachines = stateMachines;

            return fsm;
        }

        private static Motion ReplaceAnimationClip(Motion m, Dictionary<AnimationClip, AnimationClip> clipDict)
        {
            var bt = m as BlendTree;
            if (bt != null)
            {
                var children = bt.children;
                for (var i = 0; i < children.Length; i++)
                    children[i].motion = ReplaceAnimationClip(children[i].motion, clipDict);
                bt.children = children;
            }

            var clip = m as AnimationClip;
            if (clip != null)
                if (clipDict.TryGetValue(clip, out var value))
                    m = value;

            return m;
        }
    }
}
